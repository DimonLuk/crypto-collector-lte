clear:
	rm -rf **/__pycache__

reset:
	rm .image_status

clear_database:
	rm -rf __postgres_data__

init:
	@echo "" >> .image_status

database:
	docker-compose up -d database

test_database:
	docker-compose -f docker-compose.test.yml up -d database

build: clear init
ifneq ($(shell cat .image_status),DEVELOPMENT)
	docker-compose build
	@echo DEVELOPMENT > .image_status
endif

build_test: clear init
ifneq ($(shell cat .image_status),TEST)
	docker-compose -f docker-compose.test.yml build
	@echo TEST > .image_status
endif

test: build_test test_database
	docker-compose -f docker-compose.test.yml up -d
	docker-compose logs -f crypto_collector_lte

intest: build_test test_database
	docker-compose -f docker-compose.test.yml run crypto_collector_lte /bin/sh

up: build database
	docker-compose up -d

down: clear
	docker-compose down

logs:
	docker-compose logs

list:
	docker ps

restart: down up
	@echo Restarting

in: down build database
	docker-compose run -p 8000:80 crypto_collector_lte /bin/sh
