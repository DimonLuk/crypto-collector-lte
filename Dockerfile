FROM python:3.7.4-alpine3.10

ARG MODE
ENV MODE $MODE

ARG DJANGO_SETTINGS_MODULE
ENV DJANGO_SETTINGS_MODULE $DJANGO_SETTINGS_MODULE

ARG POSTGRES_NAME
ENV POSTGRES_NAME $POSTGRES_NAME

ARG POSTGRES_USER
ENV POSTGRES_USER $POSTGRES_USER

ARG POSTGRES_PASSWORD
ENV POSTGRES_PASSWORD $POSTGRES_PASSWORD

ARG POSTGRES_HOST
ENV POSTGRES_HOST $POSTGRES_HOST

ARG POSTGRES_PORT
ENV POSTGRES_PORT $POSTGRES_PORT

ARG HTTPS
ENV HTTPS $HTTPS

RUN apk add postgresql-dev
RUN apk add musl-dev
RUN apk add gcc
RUN pip install pipenv

ADD ./Pipfile /code/Pipfile
ADD ./Pipfile.lock /code/Pipfile.lock
WORKDIR /code

RUN if [ "$MODE" = "development" ] ; then pipenv install --ignore-pipfile --system --dev ; fi
RUN if [ "$MODE" = "testing" ] ; then pipenv install --ignore-pipfile --system --dev ; fi
RUN if [ "$MODE" = "production" ] ; then pipenv install --ignore-pipfile --system ; fi

ADD . /code
RUN chmod a+x ./docker-entrypoint.sh
CMD ./docker-entrypoint.sh
