import pytest
from django.core.management import call_command
from django.contrib.auth.models import User


@pytest.mark.django_db
def test_datamigrations_latest_state():
    call_command("migrate", app_label="crypto_collector_lte_app", interactive=False)
    assert len(User.objects.all()) == 1
    user = User.objects.last()
    assert user.username == "admin"
    assert user.email == "admin@example.com"
