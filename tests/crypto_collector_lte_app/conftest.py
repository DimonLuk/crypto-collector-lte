import pytest


@pytest.fixture
def tickers_data():
    return {
        "tickers": [
            {
                "sell_currency": "BTC",
                "buy_currency": "USD",
                "last": 0.0000003,
                "lowest_ask": 0.00001,
                "highest_bid": 0.00001,
                "is_frozen": False,
                "base_volume": 11111,
                "quote_volume": 11111,
                "timestamp": "2019-02-12T12:12:12Z",
            },
            {
                "sell_currency": "AWS",
                "buy_currency": "CDC",
                "last": 0.0000403,
                "lowest_ask": 0.44441,
                "highest_bid": 0.00401,
                "is_frozen": False,
                "base_volume": 22222,
                "quote_volume": 222222,
                "timestamp": "2019-04-02T12:12:12Z",
            },
        ]
    }


@pytest.fixture
def tickers_data_with_errors():
    return {
        "tickers": [
            {
                "sell_currency": "BTC",
                "buy_currency": "USD",
                "last": 0.0000003,
                "lowest_ask": 0.00001,
                "highest_bid": 0.00001,
                "is_frozen": False,
                "base_volume": 11111,
                "quote_volume": 11111,
                "timestamp": "2019-02-12T12:12:12Zaa",
            },
            {
                "sell_currency": "AWS",
                "buy_currency": "CDC",
                "last": 0.0000403,
                "lowest_ask": 0.44441,
                "highest_bid": 0.00401,
                "is_frozen": False,
                "base_volume": 22222,
                "quote_volume": "222222",
                "timestamp": "2019-04-02T12:12:12Z",
            },
        ]
    }
