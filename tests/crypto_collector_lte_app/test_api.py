import pytest
from box import Box
from crypto_collector_lte_app.serializers import TickerSerializer


@pytest.mark.django_db(transaction=True)
def test_healthcheck(app):
    response = app.get("/api/__healthcheck__/")
    assert response.status_code == 200
    response = Box(response.json())
    assert response.detail == "Everything works"


@pytest.mark.django_db(transaction=True)
def test_submit_tickers(app, tickers_data):
    response = app.post("/api/tickers/", json=tickers_data)
    response.raise_for_status()
    assert response.json() == {"detail": "Tickers were created"}


@pytest.mark.django_db(transaction=True)
def test_submit_tickers_with_error(app, tickers_data_with_errors):
    response = app.post("/api/tickers/", json=tickers_data_with_errors)
    assert response.json() == [
        {
            "timestamp": [
                "Datetime has wrong format. Use one of these formats"
                + " instead: YYYY-MM-DDThh:mm[:ss[.uuuuuu]][+HH:MM|-HH:MM|Z]."
            ]
        },
        {},
    ]


@pytest.mark.django_db(transaction=True)
def test_get_tickers(app, tickers_data):
    serializer = TickerSerializer(data=tickers_data["tickers"], many=True)
    serializer.is_valid(raise_exception=True)
    serializer.save()
    response = app.get("/api/tickers/")
    response.json() == tickers_data
