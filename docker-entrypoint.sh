if [ "$MODE" = "development" ] ; then python manage.py migrate && python manage.py runserver 0.0.0.0:80 ; fi
if [ "$MODE" = "testing" ] ; then pytest --junit-xml=test-report.xml --cov=crypto_collector_lte_app --cov-report=html --cov-report=term tests && python manage.py collectstatic --noinput ; fi
if [ "$MODE" = "production" ]; then python manage.py migrate && gunicorn -b 0.0.0.0:80 crypto_collector_lte.wsgi; fi
