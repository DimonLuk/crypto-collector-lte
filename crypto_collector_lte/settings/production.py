from .base import *  # NOQA
import logging
import os
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.integrations.logging import LoggingIntegration


MODE = "production"
DEBUG = False

sentry_logging = LoggingIntegration(
    level=logging.WARNING, event_level=logging.ERROR
)  # NOQA
sentry_sdk.init(
    dsn="https://773631890dec476aaf7652b9b7c00aa4@sentry.io/1546140",
    integrations=[DjangoIntegration(), sentry_logging],
    debug=True,
)

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": os.environ.get("POSTGRES_NAME"),
        "USER": os.environ.get("POSTGRES_USER"),
        "PASSWORD": os.environ.get("POSTGRES_PASSWORD"),
        "HOST": os.environ.get("POSTGRES_HOST"),
        "PORT": os.environ.get("POSTGRES_PORT"),
    }
}
STATIC_URL = "https://dr9ex5mzxrsp7.cloudfront.net/"
