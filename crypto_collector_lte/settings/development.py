from .base import *  # NOQA


DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": "crypto_db",
        "USER": "postgres",
        "PASSWORD": "user",
        "HOST": "database",
        "PORT": "5432",
    }
}
