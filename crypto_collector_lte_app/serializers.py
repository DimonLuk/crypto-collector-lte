from rest_framework import serializers
from .models import Ticker


class TickerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ticker
        fields = (
            "sell_currency",
            "buy_currency",
            "last",
            "lowest_ask",
            "highest_bid",
            "is_frozen",
            "base_volume",
            "quote_volume",
            "timestamp",
        )


class TickerRequestSerializer(serializers.Serializer):
    tickers = TickerSerializer(many=True)


class DjangoResponseSerializer(serializers.Serializer):
    detail = serializers.CharField(max_length=1000)
