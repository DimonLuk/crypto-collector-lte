from django.apps import AppConfig


class CryptoCollectorLteAppConfig(AppConfig):
    name = 'crypto_collector_lte_app'
