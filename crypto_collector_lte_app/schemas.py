from .serializers import TickerRequestSerializer, DjangoResponseSerializer


def get_tickers_post_schema():
    try:
        return {
            "request_body": TickerRequestSerializer,
            "responses": {201: DjangoResponseSerializer, 400: DjangoResponseSerializer},
        }
    except Exception:
        pass


def get_tickers_get_schema():
    try:
        return {
            "responses": {200: TickerRequestSerializer, 400: DjangoResponseSerializer}
        }
    except Exception:
        pass
