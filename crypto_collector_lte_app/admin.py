from django.contrib import admin
from .models import Ticker


class TickerAdmin(admin.ModelAdmin):
    pass


admin.site.register(Ticker, TickerAdmin)
