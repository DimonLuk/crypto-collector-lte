from django.db import models


class Ticker(models.Model):
    sell_currency = models.CharField(max_length=10)
    buy_currency = models.CharField(max_length=10)
    last = models.FloatField()
    lowest_ask = models.FloatField()
    highest_bid = models.FloatField()
    is_frozen = models.BooleanField()
    base_volume = models.FloatField()
    quote_volume = models.FloatField()
    timestamp = models.DateTimeField()

    def __str__(self):
        return f"{self.sell_currency}/{self.buy_currency}"
