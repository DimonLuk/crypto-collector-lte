# Generated by Django 2.2.4 on 2019-09-01 22:01

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('crypto_collector_lte_app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ticker',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sell_currency', models.CharField(max_length=10)),
                ('buy_currency', models.CharField(max_length=10)),
                ('last', models.FloatField()),
                ('lowest_ask', models.FloatField()),
                ('highest_bid', models.FloatField()),
                ('is_frozen', models.BooleanField()),
                ('base_volume', models.FloatField()),
                ('quote_volume', models.FloatField()),
                ('timestamp', models.DateTimeField()),
            ],
        ),
    ]
