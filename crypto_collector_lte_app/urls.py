from django.urls import path
from .views import healthcheck, tickers


urlpatterns = [path("__healthcheck__/", healthcheck), path("tickers/", tickers)]
