from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema
from .models import Ticker
from .serializers import TickerSerializer
from .schemas import get_tickers_post_schema, get_tickers_get_schema


@api_view(("GET",))
def healthcheck(request):
    return Response({"detail": "Everything works"}, status=status.HTTP_200_OK)


@swagger_auto_schema(method="POST", **get_tickers_post_schema())
@swagger_auto_schema(method="GET", **get_tickers_get_schema())
@api_view(("GET", "POST"))
def tickers(request):
    if request.method == "GET":
        instances = Ticker.objects.all()
        data = TickerSerializer(instances, many=True).data
        response_data = {"tickers": data}
        return Response(response_data, status=status.HTTP_200_OK)
    elif request.method == "POST":
        raw_data = request.data
        serializer = TickerSerializer(data=raw_data["tickers"], many=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        response_data = {"detail": "Tickers were created"}
        return Response(response_data, status=status.HTTP_201_CREATED)
